#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

sgdisk --zap-all /dev/nvme0n1
wipefs -a /dev/nvme0n1

timedatectl set-ntp true

sgdisk -og /dev/nvme0n1
sgdisk -n 1:0:+600MiB -c 1:"EFIBOOT" -t 1:ef00 /dev/nvme0n1
sgdisk -n 2:0:+35GiB -c 2:"root" -t 2:8304 /dev/nvme0n1
sgdisk -n 3:0:+8GiB -c 3:"/tmp" -t 3:8300 /dev/nvme0n1
sgdisk -n 4:0:+170GiB -c 4:"/home" -t 4:8302 /dev/nvme0n1

mkfs.fat -F32 /dev/nvme0n1p1
mkfs.ext4 /dev/nvme0n1p2 -F
mkfs.ext4 /dev/nvme0n1p3 -F
mkfs.ext4 /dev/nvme0n1p4 -F

mount /dev/nvme0n1p2 /mnt
mkdir -p /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot
mkdir -p /mnt/tmp
mount /dev/nvme0n1p3 /mnt/tmp
mkdir -p /mnt/home
mount /dev/nvme0n1p4 /mnt/home

echo
echo "#############################################"
echo "Disk(s) partitioned and mounted."
echo "Starting pacstrap..."
echo "#############################################"
echo

pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1

pacstrap /mnt base base-devel linux linux-firmware intel-ucode networkmanager wget rsync

genfstab -U /mnt >> /mnt/etc/fstab

echo
echo "#############################################"
echo "Entering chroot..."
echo "#############################################"
echo

curl https://gitlab.com/xnay/cais/-/raw/master/chroot.sh > /mnt/chroot.sh
arch-chroot /mnt bash ./chroot.sh && rm /mnt/chroot.sh

umount -R /mnt
